<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TC002-EnglishAndWordle</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>197fd090-03f1-4447-89ac-36d7dde6517a</testSuiteGuid>
   <testCaseLink>
      <guid>2be1c77f-e113-4a41-9ae1-5c74ff9245df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/English</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccd55251-07db-43b6-a0cb-953cd3e92988</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Wordle</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
